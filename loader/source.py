import requests


def extract_validated(date_from, date_to=None, group_by='hour'):
    # consumption ("GrossCon") in the resource is not always updated together
    # with the rest of the columns, so the last data point might be later than
    # those available at
    # https://www.energidataservice.dk/dataset/productionconsumptionsettlement
    # COALESCE needs to be there to fix `+` with Null values
    electricitybalance_url = f'''
    https://api.energidataservice.dk/datastore_search_sql?sql=
    SELECT 
        date_trunc('{group_by}', "HourUTC")  as hour_utc, 
        date_trunc('{group_by}', "HourDK")  as hour_dk,  
        round({sum_of_columns_request(["OffshoreWindLt100MW_MWh", "OffshoreWindGe100MW_MWh", "OnshoreWindLt50kW_MWh", "OnshoreWindGe50kW_MWh"])}) as wind, 
        round({sum_of_columns_request(["SolarPowerLt10kW_MWh", "SolarPowerGe10Lt40kW_MWh", "SolarPowerGe40kW_MWh", "SolarPowerSelfConMWh"])}) as solar, 
        round(sum("GrossConsumptionMWh")) as consumption
    FROM "productionconsumptionsettlement" 
    WHERE "HourUTC" >= '{date_from}' {f"""AND "HourUTC" < '{date_to}'""" if date_to else ''}
    AND "GrossConsumptionMWh" > 0
    GROUP BY 1, 2
    ORDER BY 1
    '''

    print('Requesting:\n', electricitybalance_url, '\n\n')

    return request_eds(electricitybalance_url)


def extract_non_validated(date_from, group_by='hour'):
    """
    Fetches data from EDS - electricitybalancenonv and groups it to be the same
    shape as validated
    :param group_by: granularity of time, as per postgres docs
    https://www.postgresql.org/docs/12/functions-datetime.html#FUNCTIONS
    -DATETIME-TRUNC
    :param date_from: non inclusive
    :return:
    """

    consumption_columns = [
        "OffshoreWindPower", "OnshoreWindPower", "SolarPower",
        "ExchangeGreatBelt", "ExchangeNordicCountries", "ExchangeContinent",
        "Biomass", "HydroPower", "Waste",
        "FossilGas", "FossilHardCoal", "FossilOil"
    ]

    electricitybalancenonv_ulr = f'''https://api.energidataservice.dk/datastore_search_sql?sql=
    SELECT 
        date_trunc('{group_by}', "HourUTC")  as hour_utc, 
        date_trunc('{group_by}', "HourDK")  as hour_dk, 
        round({sum_of_columns_request(["OffshoreWindPower", "OnshoreWindPower"])}) as wind, 
        round(sum("SolarPower")) AS solar,
        round({sum_of_columns_request(consumption_columns)}) AS consumption
    FROM "electricitybalancenonv" 
    WHERE "HourUTC" > '{date_from}'
    GROUP BY 1, 2
    ORDER BY 1
    '''

    print('Requesting:\n', electricitybalancenonv_ulr, '\n\n')

    return request_eds(electricitybalancenonv_ulr)


def extract_forecasts(date_from, group_by):
    forecasts_hour_url = f'''https://api.energidataservice.dk/datastore_search_sql?sql=
    SELECT 
        date_trunc('{group_by}', "HourUTC")  as hour_utc, 
        date_trunc('{group_by}', "HourDK") as hour_dk,
        round(sum(CASE WHEN "ForecastType" = 'Offshore Wind' OR "ForecastType" = 'Onshore Wind' THEN "ForecastCurrent" ELSE 0 END )) AS wind,
        round(sum((CASE WHEN "ForecastType" = 'Solar' THEN "ForecastCurrent" ELSE 0 END ))) AS solar,
        NULL AS consumption
    FROM forecasts_hour fh 
    WHERE "HourUTC" > '{date_from}'
    GROUP BY 1, 2
    ORDER BY 1
    '''

    print('Requesting:\n', forecasts_hour_url, '\n\n')

    return request_eds(forecasts_hour_url)


def extract_capacity(date_from):
    capacity_url = f"""https://api.energidataservice.dk/datastore_search_sql?sql=
    SELECT 
        "Month" as hour_dk,
        round({sum_of_columns_request(["OnshoreWindCapacity", "OffshoreWindCapacity"])}) as wind, 
        round(sum("SolarPowerCapacity")) as solar
    FROM "capacitypermunicipality"
    GROUP BY 1
    ORDER BY 1 DESC 
    LIMIT 1
    """
    print('Requesting:\n', capacity_url, '\n\n')

    return request_eds(capacity_url)


def sum_of_columns_request(columns):
    columns_prep = [f'COALESCE( sum("{column}"), 0)' for column in columns]
    return ' %2B '.join(columns_prep)


def request_eds(url):
    try:
        eds_response = requests.get(url).json()

        if eds_response.get('success', None):

            eds_records = eds_response['result']['records']

            return eds_records

        else:
            raise Exception(f'''
                Did not get successful response.
                Response: {eds_response}''')

    except Exception as e:
        raise Exception(f'Could not get any response. Exception: {e}')
