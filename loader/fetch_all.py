import calendar
import datetime as dt

import pandas as pd
import numpy as np
import pytz

from loader import source

DK_TIMEZONE = pytz.timezone('Europe/Copenhagen')

DATE_START_UTC = dt.datetime.fromisoformat(
    '2004-01-01T00:00:00+00:00').astimezone(pytz.timezone('UTC'))

DATE_NOW = dt.datetime.utcnow()
STORAGE_DIR = 'data_processing'

DATE_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S'

GRANULARITY_TO_PANDAS_GROUP_BY = {
    'hour': 'H',
    'day': 'D',
    'week': 'W-SUN',
    'month': 'MS',
    'year': 'YS'
}


def execute():
    df = pd.DataFrame()

    validated_df = fetch_validated()
    df = df.append(validated_df)

    # non-validated data
    df = df.append(fetch_non_validated(df.index[-1].strftime('%Y-%m-%d %H:%M')))

    df = df.fillna(0)

    # forecasts_hour
    df = df.append(fetch_forecasts(df.index[-1].strftime('%Y-%m-%d %H:%M'),
                                   validated_df))

    # long term forecasts
    df = df.append(create_long_term_forecast(validated_df, df.index[-1]))

    df['wind_and_solar'] = df[['wind', 'solar']].sum(axis=1)

    # group by granularity
    for granularity in GRANULARITY_TO_PANDAS_GROUP_BY.keys():
        store(df, granularity)

    print(f'Successfully saved data at: {STORAGE_DIR}')


def store(df, granularity):
    path = f'{STORAGE_DIR}/{granularity}.csv'

    if granularity == 'hour':
        # do not resample, otherwise
        # there will be zeros in places of timezone change
        grouped_df = df
    else:
        # grouping by Denmark time
        grouped_df = df.set_index('hour_dk')
        grouped_df['hour_utc'] = df.index
        grouped_df = grouped_df.resample(
            GRANULARITY_TO_PANDAS_GROUP_BY[granularity],
            label='left'
        ).agg(
            {
                'hour_utc': 'first',
                'consumption': 'sum',
                'wind': 'sum',
                'solar': 'sum',
                'wind_and_solar': 'sum',
                'type': 'max'
            })

        # change index back to hour_utc
        grouped_df.reset_index(inplace=True)
        grouped_df.set_index('hour_utc', inplace=True)

    grouped_df['green_energy_ratio'] = (grouped_df['wind_and_solar']
                                        / grouped_df['consumption']) * 100

    # round to nearest integer
    number_columns = ['consumption', 'wind', 'solar',
                      'wind_and_solar', 'green_energy_ratio', 'type']
    grouped_df[number_columns] = grouped_df[number_columns].round().astype(
        'Int32')

    # reorder columns
    grouped_df = grouped_df[['hour_dk', 'consumption', 'wind', 'solar',
                             'wind_and_solar', 'green_energy_ratio', 'type']]

    grouped_df.to_csv(path, date_format=DATE_TIME_FORMAT)


def settle_types(df, validation_type):
    df['hour_utc'] = pd.to_datetime(df['hour_utc'], utc=True)
    df['hour_dk'] = pd.to_datetime(df['hour_dk'])
    df.set_index('hour_utc', inplace=True)
    df['type'] = validation_type

    return df


def fetch_validated():
    # request years separately to make the data smaller (~9000 rows)
    past_years = pd.date_range(
        start=f'{DATE_START_UTC.year}-01-01',
        end=f'{DATE_NOW.year}-01-01', freq='YS')

    validated_df = pd.DataFrame()

    for i in range(len(past_years)):
        start = past_years[i]
        end = past_years[i + 1] if i < len(past_years) - 1 else None
        validated_data = source.extract_validated(start, end)
        validated_df = validated_df.append(
            pd.DataFrame.from_records(validated_data))

    return settle_types(validated_df, 0)


def fetch_non_validated(date_from):
    non_val_data = source.extract_non_validated(date_from)
    non_val_df = pd.DataFrame.from_records(non_val_data)

    return settle_types(non_val_df, 1)


def fetch_forecasts(date_from, validated_df):
    forecasts_data = source.extract_forecasts(date_from, 'hour')
    forecasts_df = pd.DataFrame.from_records(forecasts_data)
    forecasts_df = settle_types(forecasts_df, 2)
    forecasts_df = add_consumption_column(forecasts_df, validated_df)

    return forecasts_df


def add_consumption_column(result_df, source_df, shift_by_weekday=True):
    """
    EDS does not have consumption in forecasts resource, using
    the previous year's validated consumption
    as the current year's estimated consumption instead
    :param: forecasts_df
    :param: validated_df pandas DataFrame - validated data from which
        to get consumption
    :return: updated forecasts_df
    """

    # As consumption depends on weekday,
    # Use the previous year's validated consumption,
    # as the current year's estimated consumption,
    # mapping it by weekday
    if shift_by_weekday:
        prev_year_index = prev_year_index_by_week(result_df.index)
    else:
        prev_year = DATE_NOW.year - 1
        prev_year_index = pd.date_range(
            f'{prev_year - 1}-12-31 23:00',
            f'{prev_year}-12-31 22:00',
            freq='H', tz='UTC')

    # Get the Consumption from Validated data corresponding to the chosen dates.
    consumption_df = pd.DataFrame(
        source_df.loc[source_df.index.intersection(prev_year_index),
                      'consumption']
    )

    # Reindex in case of missing values due to summer/winter time change.
    consumption_df = consumption_df.reindex(prev_year_index)

    # update forecasts with the estimated consumption
    result_df['consumption'] = consumption_df['consumption'].values[
                               :len(result_df.index)]

    # Drop rows with NaN values.
    result_df.dropna(inplace=True)

    return result_df


def prev_year_index_by_week(current_index):
    def year_back_by_weekday(date):
        year_back = date - pd.DateOffset(years=1)
        shift = abs(date.weekday() - year_back.weekday())
        return year_back + pd.DateOffset(days=shift)

    start = year_back_by_weekday(current_index[0])
    end = year_back_by_weekday(current_index[-1])
    previous_years_dates = pd.date_range(start, end, freq='H', tz='UTC')

    return previous_years_dates


def create_long_term_forecast(validated_df, date_from):
    """
    :param date_from: Last date after which there is no data available
    :param validated_df: pandas DataFrame with validated data, will be used as
        a source of data for previous year => consumption estimates
    :return: pandas DataFrame with long term (up to the end of the year)
        estimates for wind, solar, wind_and_solar, and consumption
    """

    # Get amount of hours this year.
    bool_leap = calendar.isleap(DATE_NOW.year)
    if bool_leap:
        v_days_in_year = 366
    else:
        v_days_in_year = 365
    v_hours_in_day = 24
    v_hours_in_year = v_days_in_year * v_hours_in_day

    # Approximate the intensity of sunlight throughout a year:

    # Solar cells yield approximately 1000 hours of full effect a year.
    # Since they only generate electricity while the sun is up,
    # sine functions are used to emulate day and night.

    # Create an array of values for use in
    # a sine function based on a 24 hour period.
    # As the absolute value is used later,
    # half a period should account for one day.
    ar_used_values_day = np.array(
        [(2 * np.pi * i) / v_hours_in_day / 2 for i in range(v_hours_in_year)])
    # Values with a yearly period.
    ar_used_values_year = np.array(
        [(2 * np.pi * i) / v_hours_in_year for i in range(v_hours_in_year)])

    # Get the corresponding sine values.
    ar_sin_values_day = np.sin(
        [ar_used_values_day[i] for i in range(len(ar_used_values_day))])
    # Yearly.
    ar_sin_values_year = np.sin([ar_used_values_year[i] - np.pi / 2 for i in
                                 range(len(ar_used_values_year))])

    # Move the daily sine curve down to lengthen night time. Modify amplitude.
    ar_sin_moved_day = (abs(ar_sin_values_day) - 0.925) / 10
    # Modify the yearly sine curve.
    ar_sin_moved_year = ((ar_sin_values_year + 0.5) / 70)

    # Add them together to emulate changing daytime hours
    # depending on time of year.
    ar_sin_moved_tot = (ar_sin_moved_day + ar_sin_moved_year) * 5

    # Substitute negative values with zero, then take a root to flatten.
    ar_sin_moved_tot = np.power(
        np.array([0 if x <= 0 else x for x in ar_sin_moved_tot]), 1 / 3)

    # Add date indices for use with the intensity profile.
    ind_year_dates = pd.date_range(f'{DATE_NOW.year - 1}-12-31 23:00',
                                   f'{DATE_NOW.year}-12-31 22:00',
                                   freq='H', tz='UTC')

    # Create series of solar intensity indexed by datetime.
    df_solar_intensity = pd.DataFrame(data={'solar': ar_sin_moved_tot},
                                      index=ind_year_dates)

    # Add name to index.
    df_solar_intensity.index.rename('hour_utc', inplace=True)

    # If it is the first of the month, go two months back instead of one.
    n_months_back = 1 if DATE_NOW.day >= 2 else 2
    dt_month_back = pd.Timestamp(DATE_NOW) - pd.DateOffset(
        months=n_months_back)
    dt_month_back_first = dt.datetime(year=dt_month_back.year,
                                      month=dt_month_back.month, day=1)

    capacity_data = source.extract_capacity(dt_month_back_first)

    solar_capacity_last_month = capacity_data[0]['solar']
    wind_capacity_last_month = capacity_data[0]['wind']

    # Multiply the solar intensity profile by last month's capacity.
    df_solar_estimate = df_solar_intensity * solar_capacity_last_month

    # Estimate the effect of windmills throughout a year:

    # Windmills yield approximately 2900 hours of full effect a year.
    # This translates to roughly 8 hours of full effect a day.
    # Or 1/3 effect each whole day. This will be used in the approximation.

    # Create an array with 1/3 values of
    # the same length as the amount of hours in a year.
    ar_wind_profile = np.array([1 / 3 for i in range(v_hours_in_year)])

    # Create a dataframe of 1/3 values throughout the year.
    df_wind_intensity = pd.DataFrame(data={'wind': ar_wind_profile},
                                     index=ind_year_dates)

    # Add name to index.
    df_wind_intensity.index.rename('hour_utc', inplace=True)

    # Multiply the wind intensity profile by last month's capacity.
    df_wind_estimate = df_wind_intensity * wind_capacity_last_month

    # Create a dataframe which contains the sum of wind and solar production.
    result_df = pd.concat([df_wind_estimate, df_solar_estimate], axis=1)

    # Filter to only contain values after latest Forecast values.
    result_df = result_df[result_df.index > date_from]

    # Use the previous year's validated consumption
    # as the current year's estimated consumption.
    result_df = add_consumption_column(result_df, validated_df,
                                       shift_by_weekday=False)

    # set the type
    result_df['type'] = 3

    # add hour_dk
    result_df['hour_dk'] = result_df.index.tz_convert(
        DK_TIMEZONE).tz_localize(None)

    return result_df


if __name__ == '__main__':
    execute()
