# Green Energy

This is a script that gathers data for Green Energy Dashboard -
 https://www.energidataportal.dk/visualizations/solar-and-wind.

The data is collected from [EDS](https://energidataservice.dk/), see 
`loader/sources.py` for more.

The long-term forecasts (up to the end of the current year) 
are calculated based on previous year and last month capacity. The calculations 
are provided by Jacob Jeppesen (Energinet).

## Links to related repos

* Visualization React app (most of the viz behavior and in-browser logic):
  * https://dev.azure.com/energinet/EDP/_git/solar-and-wind-visualization
  * https://gitlab.com/datopian/eds/green-energy-dashboard
* EDP theme (a frontend-v2 theme for https://energidataservice.dk/ - styles and page layout are there):
  * https://dev.azure.com/energinet/EDP/_git/EDP 
  * https://gitlab.com/datopian/eds/frontend-edp

## Contributing

The script is written for Python 3.7. There are different dependency files, depending on what you want to do.

### Running locally

Install dependencies (`pip install -r requirements.txt`)

For testing purposes you can run `python loader/fetch_all.py` which will copy 
create data in `data_processing` folder - which is not used by the Dashboard.

If you want to push changes later, run `python runner.py` which will also copy 
the result to `data` folder.

### Run locally and store data to Redis

You'll need to have locally running Redis server. On how to setup Redis, see https://redis.io/topics/quickstart

To fetch needed data and store to Redis, run `python store_toredis.py`, which is doing almost the same as `python runner.py`. Only one difference is that data will be stored in Redis cache in binary format.

Data can be accesed using `redis-cli` command, ex. `redis-cli get hour`. We are storing data using the following Redis keys: `hour`, `day`, `week`, `month`, `year` and `datapackage`.

### Develop

Install development dependencies:

```bash
pip install -r dev-requirements.txt
```

Write tests with [pytest](https://docs.pytest.org/en/latest/contents.html
) and [tox](https://tox.readthedocs.io/en/latest/).

**Important!** It's better to not to push any local changes to `data` folder
because gitlab ci also updates and commits to it so there can be conflicts.

### Serve files for the frontend (without using GitLab) for testing purposes

There is also `python app.py` which launches a simple flask server, present 
for historical reasons. Can be used for development purposes, not optimized for 
production. Has it's own dependencies in `server-requirements.txt`

```bash
pip install -r server-requirements.txt
```

## Deployment

Currently the script is executed by cron every hour. The resulting data is served as files directly from the GitLab repository. 
For more details see `.gitlab-ci.yml` for more info.
