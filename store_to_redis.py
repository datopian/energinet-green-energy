from loader import fetch_all
from distutils.dir_util import copy_tree
from dotenv import load_dotenv, find_dotenv
import os
import redis
import json

load_dotenv()

TMP_RESULT_DIR = 'data_processing'

REDIS_STORE_URL = os.environ.get('REDIS_SOLAR_AND_WIND')

GRANULARITY = [
    'hour',
    'day',
    'week',
    'month',
    'year'
]


if __name__ == '__main__':
    # try to get all the data
    fetch_all.execute()

    r =  redis.StrictRedis.from_url(url=REDIS_STORE_URL)

    # Storing data to Redis in binary format
    for item in GRANULARITY:
        csv_file = open(f'{TMP_RESULT_DIR}/{item}.csv',"rb").read()
        r.set(item, csv_file)

    with open('datapackage.json', encoding='utf-8-sig') as json_file:
        json_data = json.load(json_file)
        data = json.dumps(json_data)

    r.set('datapackage', data)

    # Uncomment this if Redes uses RDB snapshot.
    # This settings have to be applied in 6379.conf:
    #     dbfilename        dump.rdb
    #     dir               ./
    #     rdbcompression    yes

    #r.bgsave() #to uncomment

    print(f'Successfully copied data to Redis')