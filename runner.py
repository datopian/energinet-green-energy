from loader import fetch_all
from distutils.dir_util import copy_tree

RESULT_DIR = 'data'

if __name__ == '__main__':
    # try to get all the data
    fetch_all.execute()

    # if no exceptions - copy data into result folder
    copy_tree(fetch_all.STORAGE_DIR, RESULT_DIR)

    print(f'Successfully copied data to {RESULT_DIR}')