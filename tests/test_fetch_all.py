import pandas as pd

from loader import fetch_all


def test_prev_year_index_by_week():
    current_index = pd.date_range('2020-02-28T00:00:00',
                                  '2020-03-01T00:00:00',
                                  freq='H', tz='UTC')
    result_index = fetch_all.prev_year_index_by_week(current_index)
    expected_index = pd.date_range('2019-03-01 00:00:00',
                                   '2019-03-03 00:00:00',
                                   freq='H', tz='UTC')

    assert result_index.equals(expected_index)


def test_store_by_year_adds_correct_type(all_types_df, tmp_path):
    fetch_all.store(all_types_df, 'year')
    result = pd.read_csv(f'{tmp_path}/year.csv')
    assert result.iloc[0]['type'] == 3


def test_group_by_aggregates_correctly(all_types_df, tmp_path):
    fetch_all.store(all_types_df, 'year')
    result = pd.read_csv(f'{tmp_path}/year.csv')
    assert result.iloc[0]['consumption'] == 4444
    assert result.iloc[0]['wind'] == 4444
    assert result.iloc[0]['solar'] == 4444
    assert result.iloc[0]['wind_and_solar'] == 8888
    assert result.iloc[0]['green_energy_ratio'] == 200
    assert result.iloc[0]['type'] == 3


def test_fetch_validated_adds_type_0(extract_all):
    result = fetch_all.fetch_validated()

    assert result.iloc[0]['type'] == 0


def test_fetch_non_validated_adds_type_1(extract_all):
    result = fetch_all.fetch_non_validated('some')

    assert result.iloc[0]['type'] == 1


def test_fetch_forecasts_adds_type_2(extract_all, validated_df):
    result = fetch_all.fetch_forecasts('some', validated_df)

    assert result.iloc[0]['type'] == 2


def test_create_long_term_forecast_adds_type_3(validated_df, extract_capacity):
    result = fetch_all.create_long_term_forecast(
        validated_df,
        pd.Timestamp('2020-02-28T00:00:00+00:00'))

    assert result.iloc[0]['type'] == 3
