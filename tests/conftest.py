import pytest
import pandas as pd

from loader import fetch_all


@pytest.fixture(autouse=True)
def storage_dir(monkeypatch, tmp_path):
    monkeypatch.setattr(fetch_all, "STORAGE_DIR", tmp_path)


@pytest.fixture
def extract_all(monkeypatch):
    def mock_function(date_from, date_to=None, group_by='hour'):
        return [
            {'hour_utc': '2020-01-01T00:00:00',
             'hour_dk': '2020-01-01T01:00:00',
             'consumption': 1111,
             'solar': 1111,
             'wind': 1111},
            {'hour_utc': '2020-01-01T01:00:00',
             'hour_dk': '2020-01-01T02:00:00',
             'consumption': 1111,
             'solar': 1111,
             'wind': 1111},
        ]

    monkeypatch.setattr(fetch_all.source, "extract_validated", mock_function)
    monkeypatch.setattr(fetch_all.source, "extract_non_validated",
                        mock_function)
    monkeypatch.setattr(fetch_all.source, "extract_forecasts",
                        mock_function)


@pytest.fixture
def extract_capacity(monkeypatch):
    capacity = [{'hour_dk': '2020-04-01', 'solar': 1128.0, 'wind': 6182.0}]

    def mock_function(_):
        return capacity

    monkeypatch.setattr(fetch_all.source, "extract_capacity",
                        mock_function)


@pytest.fixture
def validated_df():
    return pd.read_pickle('tests/data/validated_df.pkl')


@pytest.fixture
def all_types_df():
    return pd.DataFrame(
        [
            {
                'hour_dk': pd.Timestamp('2020-01-01T01:00:00'),
                'consumption': 1111,
                'solar': 1111,
                'wind': 1111,
                'wind_and_solar': 2222,
                'type': 0,
            },
            {
                'hour_dk': pd.Timestamp('2020-01-01T02:00:00'),
                'consumption': 1111,
                'solar': 1111,
                'wind': 1111,
                'wind_and_solar': 2222,
                'type': 1,
            },
            {
                'hour_dk': pd.Timestamp('2020-01-01T03:00:00'),
                'consumption': 1111,
                'solar': 1111,
                'wind': 1111,
                'wind_and_solar': 2222,
                'type': 2,
            },
            {
                'hour_dk': pd.Timestamp('2020-01-01T04:00:00'),
                'consumption': 1111,
                'solar': 1111,
                'wind': 1111,
                'wind_and_solar': 2222,
                'type': 3,
            },
        ],
        index=pd.DatetimeIndex([
            '2020-01-01T00:00:00+00:00',
            '2020-01-01T01:00:00+00:00',
            '2020-01-01T02:00:00+00:00',
            '2020-01-01T03:00:00+00:00',
        ])
    )
