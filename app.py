import json
from distutils.dir_util import copy_tree
from flask_cors import CORS
import atexit

from apscheduler.schedulers.background import BackgroundScheduler

from dataflows import Flow, load
from flask import Flask, send_from_directory, make_response

from loader import fetch_all

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


def refresh_data():
    try:
        # try to get all the data
        fetch_all.execute()

        # if no exceptions - copy data into result folder
        copy_tree(fetch_all.STORAGE_DIR, 'data')
    except Exception as e:
        print(e)


@app.route('/data/<path:granularity>.csv')
def data(granularity):
    with open(f'data/{granularity}.csv') as f:
        text = ''.join(f.readlines())

    response = make_response(text, 200)
    response.mimetype = "text/plain"
    return response


@app.route('/datapackage.json')
def datapackage():
    with open(f'datapackage.json') as f:
        text = ''.join(f.readlines())

    response = make_response(text, 200)
    response.mimetype = "application/json"
    return response


# scheduler = BackgroundScheduler()
# scheduler.add_job(func=refresh_data, trigger="cron", minute='5')
# scheduler.start()
#
# # Shut down the scheduler when exiting the app
# atexit.register(lambda: scheduler.shutdown())

if __name__ == "__main__":
    app.run(debug=True, port=8385)
